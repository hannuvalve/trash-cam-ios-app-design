//
//  ProductResponse.swift
//  scanner
//
//  Created by Anh Duong on 27.8.2019.
//  Copyright © 2019 Anh Duong. All rights reserved.
//

struct ProductResponse : Codable {
    var code: String
    var product: Product
    var status: Int
}
