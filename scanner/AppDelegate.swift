//
//  AppDelegate.swift
//  scanner
//
//  Created by Anh Duong on 19/08/2019.
//  Copyright © 2019 Anh Duong. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let navgiationController = UINavigationController()
        let mainViewController = HomeViewController()
        navgiationController.pushViewController(mainViewController, animated: false)
        window!.rootViewController = navgiationController
        window!.makeKeyAndVisible()
        return true
    }

}

